/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geneticalgorithm;

/**
 *
 * @author svman4
 */
public abstract class FitnessFunction {

    /**
     *
     * @param chromo
     * @return
     */
    public abstract float calculate(Chromosome chromo);
    
} // end of FitnessFuction()