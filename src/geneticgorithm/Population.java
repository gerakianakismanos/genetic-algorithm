/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geneticalgorithm;

import java.util.Arrays;
import java.util.Random;

/**
 *
 * @author svman4
 * @param <T> 
 * @param <F>
 *
 */
public class Population<T extends Chromosome, F extends FitnessFunction> {

    private T chromosomes[];
    private F fitness;

    @SafeVarargs
    static <E> E[] newArray(int length, E... array) {
        return Arrays.copyOf(array, length);
    }

    public Population(int chromosome_length) {
        chromosomes = newArray(chromosome_length);
//        chromosomes = new T[chromosome_length];
    } // end of constructor(int)

    public T[] getChromosomes() {
        return (T[]) chromosomes;
    } // end of getChromosomes

    public void setFitnessFunction(F function) {
        fitness = function;
    } // end of setFitnessFunction(FitnessFunction)

    public void setChromosomes(T[] chromosomes) {
        this.chromosomes = chromosomes;
    } // end of setChromosomes(Chromosome[])

    /**
     * Αξιολογεί το πληθυσμό.
     */
    public void evaluatePopulation() {
        if (chromosomes == null) {
            throw new NullPointerException("Δεν έχει δημιουργηθεί ο πίνακας με τα χρωμοσώματα.");
        }
        if (fitness == null) {
            throw new NullPointerException("Δεν έχει τοποθετηθεί μηχανή αξιολόγησης.");
        }
        float eval;
        for (Chromosome chromosome : chromosomes) {
            eval = fitness.calculate(chromosome);
            chromosome.setEvaluation(eval);
        }
    } // end evaluatePopulation()

    /**
     * Ταξινομεί τα χρωμοσώματα.
     */
    public void sort() {
        Arrays.sort(chromosomes, (Chromosome chromo1, Chromosome chromo2) -> {
            if (chromo1.getEvaluation() < chromo2.getEvaluation()) {
                return 1;
            }
            return -1;
        });
    } // end of selection()

    /**
     * Δημιουργεί τυχαία χρωμοσώματα στο πληθυσμό.<br> Όλα τα γονίδια απο
     * προηγούμενες αναζητήσεις θα χαθούν.
     */
    public void randomizePopulation() {
        Random random = new Random();
        random.setSeed(System.currentTimeMillis());
        randomizePopulation(random);
    } // end of randomizePopulation()

    /**
     * Μεταλλάσει τον πληθυσμό.
     *
     * @param random Η γεννήτρια τυχαίων αριθμών που θα χρησιμοποιηθεί.
     * @param percentOfMutation Το ποσοστό της μετάλλαξης.
     */
    public void mutatePopulation(Random random, float percentOfMutation) {
        if (chromosomes == null) {
            throw new NullPointerException("Δεν έχει δημιουργηθεί ο πίνακας με τα χρωμοσώματα.");
        }
        for (Chromosome chromo : chromosomes) {
            chromo.mutation(random, percentOfMutation);
        }
    } // end of mutatePopulation(Random,float)

    /**
     * Επιστρέφει το μέγεθος του πληθυσμού
     *
     * @return
     */
    public int getPopulationSize() {
        return chromosomes.length;
    } // end of getPopulationSize()

    /**
     * Επιστρέφει ένα πίνακα που περιέχει την αξιολόγηση για κάθε χρωμόσωμα του
     * πληθυσμού.
     *
     * @return
     */
    public float[] getEvaluationTable() {
        float eval[] = new float[chromosomes.length];
        for (int i = 0; i < chromosomes.length; i++) {
            eval[i] = chromosomes[i].getEvaluation();
        }
        return eval;
    } // end of getEvaluationTabel()

    /**
     * Δημιουργεί τυχαία χρωμοσώματα στο πληθυσμό.<br> Όλα τα γονίδια απο
     * προηγούμενες αναζητήσεις θα χαθούν.
     *
     * @param random Η γεννήτρια τυχαίων αριθμών που θα χρησιμοποιηθεί για την
     * δημιουργία τυχαίων χρωμοσωμάτων.
     */
    public void randomizePopulation(Random random) {
        if (chromosomes == null) {
            throw new NullPointerException("Δεν έχει δημιουργηθεί ο πίνακας με τα χρωμοσώματα.");
        }
        for (Chromosome chromosome : chromosomes) {
            chromosome.randomize(random);
        }
    } // end of randomizePopulation(Random)
} // end of class Population
