/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geneticalgorithm;

import java.util.Random;

/**
 *
 * @author svman4
 */
public class Chromosome {

    /**
     * Περιέχει τα γονίδια γι αυτό το χρωμόσωμα.
     */
    public int gene[];

    /**
     * Η αξία του χρωμοσώματος.
     */
    private float evaluation;

    public Chromosome(int genelength) {
        gene = new int[genelength];
    } // end of constructor(int)

    /**
     * Τοποθετεί τυχαίες τιμές στα γονίδια, χρησιμοποιώντας την γεννήτρια
     * τυχαίων αριθμών που δίνεται.
     *
     * @param random
     */
    public void randomize(Random random) {
        if (gene == null) {
            throw new NullPointerException("Δεν έχει δημιουργηθεί ο πίνακας με τα γονίδια.");
        }
        for (int i = 0; i < gene.length; i++) {
            gene[i] = (random.nextBoolean() ? 1 : 0);
        }
    } // end of randomize(Random)

    /**
     * Μεταλλάσει το χρωμόσωμα.
     *
     * @param random Η γεννήτρια τυχαίων αριθμών που χρησιμοποιείται.
     * @param mutationPercent Το ποσοστό της μετάλλαξης που θέλουμε.
     */
    public void mutation(Random random, float mutationPercent) {
        if (gene == null) {
            throw new NullPointerException("Δεν έχει δημιουργηθεί ο πίνακας με τα γονίδια.");
        }
        for (int i = 0; i < gene.length; i++) {
            if ((random.nextFloat() < mutationPercent)) {
                if (gene[i] == 1) {
                    gene[i] = 0;
                } else {
                    gene[i] = 1;
                }
            }
        }
    } // end of mutation

    void setEvaluation(float eval) {
        evaluation = eval;
    } // end of setEvaluation(float)

    public float getEvaluation() {
        return evaluation;
    } // end of getEvaluation()
} // end of class Chromosome
